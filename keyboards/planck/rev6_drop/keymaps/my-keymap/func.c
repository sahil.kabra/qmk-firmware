#include QMK_KEYBOARD_H
#include "constants.h"
#include "func.h"


bool should_handle(keyrecord_t *record) {
  return record->event.pressed && get_mods() && MOD_MASK_GUI;
}

void send(const char *l_keys) {
    SEND_STRING(l_keys);
}

bool process_func_record_user(uint16_t keycode, keyrecord_t *record, bool is_mac) {
  switch(keycode) {
    case KC_F1:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F1))));
        return false;
      }
      break;
    case KC_F2:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F2))));
        return false;
      }
      break;
    case KC_F3:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F2))));
        return false;
      }
      break;
    case KC_F4:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F4))));
        return false;
      }
      break;
    case KC_F5:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F5))));
        return false;
      }
      break;
    case KC_F6:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F6))));
        return false;
      }
      break;
    case KC_F7:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F7))));
        return false;
      }
      break;
    case KC_F8:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F8))));
        return false;
      }
      break;
    case KC_F9:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F9))));
        return false;
      }
      break;
    case KC_F10:
      if (should_handle(record)) {
        send(SS_LCTL(SS_LALT(SS_TAP(X_F10))));
        return false;
      }
      break;
  }

  return true;
}
