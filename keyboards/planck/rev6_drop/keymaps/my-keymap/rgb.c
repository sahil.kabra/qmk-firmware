#include QMK_KEYBOARD_H
#include "rgb.h"
#include "constants.h"

void layer_set_rgb(uint32_t state) {
    switch(biton32(state)) {
        case _NUMPAD:
            rgblight_sethsv_noeeprom(HSV_GREEN);
            break;
        case _SYMBOLS:
            rgblight_sethsv_noeeprom(HSV_BLUE);
            break;
        case _FUNC:
            rgblight_sethsv_noeeprom(HSV_PURPLE);
            break;
        case _UTIL:
            rgblight_sethsv_noeeprom(HSV_ORANGE);
            break;
        case _WORDS:
            rgblight_sethsv_noeeprom(HSV_RED);
            break;
        default:
            switch (biton32(default_layer_state)) {
                case _QWERTY:
                    rgblight_sethsv_noeeprom(HSV_MAGENTA);
                    break;
                case _DVORAK:
                    rgblight_sethsv_noeeprom(HSV_YELLOW);
                    break;
                default:
                    rgblight_sethsv_noeeprom(HSV_WHITE);
                    break;
            }
    }
}
