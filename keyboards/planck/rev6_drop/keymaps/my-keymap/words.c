#include QMK_KEYBOARD_H
#include "constants.h"
#include "words.h"

bool send_normal(keyrecord_t *record, char value[]) {
  if (record->event.pressed) {
      SEND_STRING(value);
      return false;
  }
  return true;
}

bool shift_pressed(void) {
    return get_mods() & MOD_BIT(KC_LSFT) ||
        get_mods() & MOD_BIT(KC_RSFT);
}

bool send_shifted(keyrecord_t *record, char value[], char sValue[]) {
  if (shift_pressed()) {
      unregister_code(KC_LSFT);
      unregister_code(KC_RSFT);
      return send_normal(record, sValue);
  } else {
      return send_normal(record, value);
  }
  return true;
}

bool process_word_record_user(uint16_t keycode, keyrecord_t *record) {
    switch(keycode) {
        case KC_A:
          return send_normal(record, "Apple");
          break;
        case KC_C:
          return send_normal(record, "Skyfii");
          break;
        case KC_D:
          return send_normal(record, "Dog");
          break;
        case KC_H:
          return send_normal(record, "Home");
          break;
        case KC_P:
          return send_shifted(record, "pass", "Partition");
          break;
        case KC_R:
          return send_normal(record, "Root");
          break;
        case KC_S:
          return send_normal(record, "Sunday");
          break;
        case KC_T:
          return send_normal(record, "This is the ");
          break;
    }

    return true;
}
