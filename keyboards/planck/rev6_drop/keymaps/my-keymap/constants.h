#pragma once

#include QMK_KEYBOARD_H

enum planck_layers {
  _DVORAK,
  _QWERTY,
  _TEST,
  _NUMPAD,
  _SYMBOLS,
  _FUNC,
  _WORDS,
  _PLOVER,
  _UTIL,
};

enum planck_keycodes {
  QWERTY = SAFE_RANGE,
  DVORAK,
  PLOVER,
  UTIL,
  EXT_PLV,
  SYMBOLS,
  NUMPAD,
  FUNC,
  TEST,
  MAC,
};

enum tap_dance_keycodes {
    TD_SP_NM = 0,
    TD_SU_FN,
    TD_LF_HOME,
    TD_RT_END,
    TD_SH_CP,
    TD_TEST,
};

#define LT_SP_NU LT(_NUMPAD, KC_SPACE)
#define LT_SP_SM LT(_SYMBOLS, KC_SPACE)
#define LT_ES_FN LT(_FUNC, KC_ESC)

