/* Copyright 2015-2017 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include "muse.h"
#include "constants.h"
#include "rgb.h"
#include "words.h"
#include "func.h"

tap_dance_action_t tap_dance_actions[] = {
    [TD_SP_NM]   = ACTION_TAP_DANCE_LAYER_TOGGLE(KC_SPACE, _NUMPAD),
    [TD_SU_FN]   = ACTION_TAP_DANCE_LAYER_TOGGLE(KC_LCMD, _FUNC),
    [TD_LF_HOME] = ACTION_TAP_DANCE_DOUBLE(KC_LEFT, KC_HOME),
    [TD_RT_END]  = ACTION_TAP_DANCE_DOUBLE(KC_RGHT, KC_END),
    [TD_SH_CP]   = ACTION_TAP_DANCE_DOUBLE(KC_LSFT, KC_CAPS),
    [TD_TEST]    = ACTION_TAP_DANCE_DOUBLE(KC_A, KC_B),
};

float mac_toggle_song[][2] = SONG(COIN_SOUND);
bool is_mac = false;

enum unicode_names {
    BANG,
    IRONY,
    SNEK
};

const uint32_t PROGMEM unicode_map[] = {
    [BANG]  = 0x203D,
    [IRONY] = 0x2E2E,
    [SNEK]  = 0x1F40D,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Dvorak
 * ,-----------------------------------------------------------------------------------.
 * |  ;   |   ,  |   .  |   P  |   Y  |  Tab | Bksp |   F  |   G  |   C  |   R  |  L   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |  A   |   O  |   E  |   U  |   I  | Ctrl |   /  |   D  |   H  |   T  |   N  |  S   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |  '   |   Q  |   J  |   K  |   X  | CSh  | Entr |   B  |   M  |   W  |   V  |  Z   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * | Esc  | Adjst| Alt  | CMD  | Nmpd |  Spc | Spc  | Symb | Left | Up   | Down |Right |
 * `-----------------------------------------------------------------------------------'
 */
[_DVORAK] = LAYOUT_planck_grid(
 KC_SCLN,  KC_COMM, KC_DOT,  KC_P,    KC_Y,     KC_TAB,       KC_BSPC,         KC_F,     KC_G,           KC_C,  KC_R,    KC_L,
 KC_A,     KC_O,    KC_E,    KC_U,    KC_I,     KC_LCTL,      RCTL_T(KC_SLSH), KC_D,     KC_H,           KC_T,  KC_N,    KC_S,
 KC_QUOT,  KC_Q,    KC_J,    KC_K,    KC_X,     TD(TD_SH_CP), RSFT_T(KC_ENT),  KC_B,     KC_M,           KC_W,  KC_V,    KC_Z,
 LT_ES_FN, UTIL,    KC_LALT, KC_LCMD, LT_SP_NU, LT_SP_SM,     TD(TD_SP_NM),    LT_SP_SM, TD(TD_LF_HOME), KC_UP, KC_DOWN, TD(TD_RT_END)
),

/* Qwerty
 * ,-----------------------------------------------------------------------------------.
 * |   Q  |   W  |   E  |   R  |   Y  |  Tab | Bksp |   Y  |   U  |   I  |   O  |  P   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |   A  |   S  |   D  |   F  |   G  | Ctrl | 'Ct  |   H  |   J  |   K  |   L  |  ;   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |   Z  |   X  |   C  |   V  |   B  | CpSh | EnSh |   N  |   M  |   ,  |   ,  |  /   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * | Esc  | Adjst| Alt  | SuFn | Nmpd |  Spc | Spc  | Symb | Left | Down |  Up  |Right |
 * `-----------------------------------------------------------------------------------'
 */
[_QWERTY] = LAYOUT_planck_grid(
    KC_Q,     KC_W, KC_E,    KC_R,    KC_T,     KC_TAB,          KC_BSPC,         KC_Y,     KC_U,           KC_I,    KC_O,    KC_P,
    KC_A,     KC_S, KC_D,    KC_F,    KC_G,     KC_LCTL,         RCTL_T(KC_QUOT), KC_H,     KC_J,           KC_K,    KC_L,    KC_SCLN,
    KC_Z,     KC_X, KC_C,    KC_V,    KC_B,     LSFT_T(KC_CAPS), RSFT_T(KC_ENT),  KC_N,     KC_M,           KC_COMM, KC_DOT,  KC_SLSH,
    LT_ES_FN, UTIL, KC_LALT, KC_LCMD, LT_SP_NU, LT_SP_SM,        TD(TD_SP_NM),    LT_SP_SM, TD(TD_LF_HOME), KC_UP,   KC_DOWN, TD(TD_RT_END)
),

/* Numpad
 * ,-----------------------------------------------------------------------------------.
 * | BANG | SNEK | IRONY|      |      | TRNS | TRNS |   (  |   1  |  2   |  3   |  +   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      | INS  | INS  |      |      | TRNS | TRNS |   )  |   4  |  5   |  6   |  -   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      | TRNS | TRNS |      |   7  |  8   |  9   |  *   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * | TRNS | TRNS | TRNS | TRNS |      | TRNS | TRNS |      |   0  |  =   |  .   |  /   |
 * `-----------------------------------------------------------------------------------'
 */
[_NUMPAD] = LAYOUT_planck_grid(
    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO, KC_TRNS, KC_TRNS, KC_LPRN, KC_1, KC_2,        KC_3,   KC_KP_PLUS,
    KC_NO,   KC_INS,  KC_INS,  KC_NO,   KC_NO, KC_TRNS, KC_TRNS, KC_RPRN, KC_4, KC_5,        KC_6,   KC_KP_MINUS,
    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO, KC_TRNS, KC_TRNS, KC_NO,   KC_7, KC_8,        KC_9,   KC_ASTR,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_NO, KC_TRNS, KC_TRNS, KC_TRNS, KC_0, KC_KP_EQUAL, KC_DOT, KC_KP_SLASH
),

/* Symbols
 * ,------------------------------------------------------------------------------------.
 * |  ;   |  ,   |   .  |   ~  |  %   | TRNS | TRNS |  -   |  `   |  /   |  @   |   \   |
 * |------+------+------+------+------+------+------+------+------+------+------+-------|
 * |  $   |  &   |   [  |   {  |  }   |  (   |  =   |  *   |  )   |  +   |  ]   |   !   |
 * |------+------+------+------+------+------+------+------+------+------+------+-------|
 * |  '   |  "   |   :  |   <  |  >   | TRNS | TRNS |  _   |  #   |  ?   |  ^   |   |   |
 * |------+------+------+------+------+------+------+------+------+------+------+-------|
 * |      |      |      |      |      | TRNS | TRNS |      |      |      |      |       |
 * `------------------------------------------------------------------------------------'
 */
[_SYMBOLS] = LAYOUT_planck_grid(
    KC_SCLN, KC_COMM, KC_DOT,    KC_TILD, KC_PERC, KC_TRNS, KC_DEL,   KC_MINS,  KC_GRV,  KC_SLSH,  KC_AT,   KC_BSLS,
    KC_DLR,  KC_AMPR, KC_LBRC,   KC_LCBR, KC_RCBR, KC_LPRN, KC_EQUAL, KC_ASTR,  KC_RPRN, KC_PLUS,  KC_RBRC, KC_EXLM,
    KC_QUOT, KC_DQT,  KC_COLN,   KC_LT,   KC_GT,   KC_TRNS, KC_TRNS,  KC_UNDS,  KC_HASH, KC_QUES,  KC_CIRC, KC_PIPE,
    KC_NO,   KC_NO,   KC_NO,     KC_NO,   KC_NO,   KC_NO,   KC_NO,    KC_NO,    KC_NO,   KC_NO,    KC_NO,   KC_NO
),

/* Function
 * ,-----------------------------------------------------------------------------------.
 * |      |      |      |      |      | TRNS | TRNS |      |  F1  | F2   | F3   |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      | TRNS | TRNS |      |  F4  | F5   | F6   |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      | TRNS | TRNS |      |  F7  | F8   | F9   |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * | TRNS | TRNS | TRNS | TRNS |      | TRNS | TRNS |      |  F10 | F11  | F12  |      |
 * `-----------------------------------------------------------------------------------'
 */
[_FUNC] = LAYOUT_planck_grid(
    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO, KC_TRNS, KC_TRNS, KC_NO, KC_F1,  KC_F2,  KC_F3,  KC_NO,
    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO, KC_TRNS, KC_TRNS, KC_NO, KC_F4,  KC_F5,  KC_F6,  KC_NO,
    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO, KC_TRNS, KC_TRNS, KC_NO, KC_F7,  KC_F8,  KC_F9,  KC_NO,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_NO, KC_TRNS, KC_TRNS, KC_NO, KC_F10, KC_F11, KC_F12, KC_NO
),

/*
 * Common words
 */
[_WORDS] = LAYOUT_planck_grid(
    KC_NO,   KC_NO,   KC_NO,   KC_R,    KC_T,     KC_TRNS,  KC_TRNS,      KC_NO,    KC_NO,   KC_I,        KC_NO,       KC_P,
    KC_A,    KC_S,    KC_D,    KC_NO,   KC_NO,    KC_AT,    KC_NO,        KC_H,     KC_NO,   KC_NO,       KC_NO,       KC_NO,
    KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,     KC_LSFT,  KC_LSFT,      KC_NO,    KC_NO,   KC_NO,       KC_NO,       KC_NO,
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, LT_SP_NU, LT_SP_SM, TD(TD_SP_NM), LT_SP_SM, KC_TRNS, TO(_DVORAK), TO(_QWERTY), KC_TRNS
),

/* Testing
 */
[_TEST] = LAYOUT_planck_grid(
    KC_NO,   KC_NO,   KC_NO,     KC_NO,   KC_NO,  KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_NO,   KC_NO,  KC_NO,
    KC_NO,   KC_NO,   KC_NO,     KC_NO,   KC_NO,  KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_NO,   KC_NO,  KC_NO,
    KC_NO,   KC_NO,   KC_NO,     KC_NO,   KC_NO,  KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_NO,   KC_NO,  KC_NO,
    KC_TRNS, KC_TRNS, KC_TRNS,   KC_TRNS, KC_NO,  KC_TRNS, KC_TRNS, KC_NO,  KC_NO,  KC_NO,   KC_NO,  KC_NO
),


/* Plover layer (http://opensteno.org)
 * ,-----------------------------------------------------------------------------------.
 * |   #  |   #  |   #  |   #  |   #  |   #  |   #  |   #  |   #  |   #  |   #  |   #  |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |   S  |   T  |   P  |   H  |   *  |   *  |   F  |   P  |   L  |   T  |   D  |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |   S  |   K  |   W  |   R  |   *  |   *  |   R  |   B  |   G  |   S  |   Z  |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * | Exit |      |      |   A  |   O  |             |   E  |   U  |      |      |      |
 * `-----------------------------------------------------------------------------------'
 */
[_PLOVER] = LAYOUT_planck_grid(
    KC_1,    KC_1,    KC_1,    KC_1,    KC_1,    KC_1,    KC_1,    KC_1,    KC_1,    KC_1,    KC_1,    KC_1   ,
    XXXXXXX, KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC,
    XXXXXXX, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,
    EXT_PLV, XXXXXXX, XXXXXXX, KC_C,    KC_V,    XXXXXXX, XXXXXXX, KC_N,    KC_M,    XXXXXXX, XXXXXXX, XXXXXXX
),

/* Adjust
 *                      v------------------------RGB CONTROL--------------------v
 * .-----------------------------------------------------------------------------------.
 * |      |      |Debug | RGB  |RGBMOD| HUE+ | HUE- | SAT+ | SAT- |BRGTH+|BRGTH-|  Del |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |MUSmod|Aud on|Audoff|AGnorm|AGswap|      |      |      |Plover|      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |Voice-|Voice+|Mus on|Musoff|MIDIon|MIDIof|TermOn|TermOf|      |      |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |             |      |      | Reset|Qwerty|Dvork |
 * `-----------------------------------------------------------------------------------'
[_ADJUST] = LAYOUT_planck_grid(
    _______, _______, DEBUG,   RGB_TOG, RGB_MOD, RGB_HUI, RGB_HUD, RGB_SAI, RGB_SAD,  RGB_VAI, RGB_VAD, KC_DEL,
    _______, _______, MU_MOD,  AU_ON,   AU_OFF,  AG_NORM, AG_SWAP, _______, _______,  _______, PLOVER,  _______,
    _______, MUV_DE,  MUV_IN,  MU_ON,   MU_OFF,  MI_ON,   MI_OFF,  TERM_ON, TERM_OFF, _______, _______, _______,
    _______, _______, _______, _______, _______, _______, _______, _______, _______,  RESET,   QWERTY,  DVORAK
)
*/

 /* util
 * .-----------------------------------------------------------------------------------.
 * |      |      |      |      |      |      |      |      |      |      |      |Words |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |RGBTog|Brgth+|Brgth-|
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |  mac |      |Audon |Audoff|
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |  xxx |      |      |      |      |      |      |      | Dvork|Qwerty|DFU   |
 * `-----------------------------------------------------------------------------------'
 */
[_UTIL] = LAYOUT_planck_grid(
    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,       KC_NO,       TO(_WORDS),
    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   RGB_TOG,     RGB_VAI,     RGB_VAD,
    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   MAC,     KC_NO,       AU_ON,       AU_OFF,
    _______, _______, _______, _______, _______, _______, _______, _______, _______, DF(_DVORAK), DF(_QWERTY), QK_BOOT
)
};

#ifdef AUDIO_ENABLE
  float plover_song[][2]     = SONG(PLOVER_SOUND);
  float plover_gb_song[][2]  = SONG(PLOVER_GOODBYE_SOUND);
#endif

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (layer_state_is(_WORDS)) {
        return process_word_record_user(keycode, record);
    } else if (layer_state_is(_FUNC)) {
        return process_func_record_user(keycode, record, is_mac);
    } else {
      switch (keycode) {
        case MAC:
          if (record->event.pressed) {
              is_mac = !is_mac;
              PLAY_SONG(mac_toggle_song);
          }
          return false;
          break;
        case UTIL:
          if (record->event.pressed) {
              layer_on(_UTIL);
          } else {
              layer_off(_UTIL);
          }
          return false;
          break;
        case PLOVER:
          if (record->event.pressed) {
            #ifdef AUDIO_ENABLE
              stop_all_notes();
              PLAY_SONG(plover_song);
            #endif
            layer_off(_SYMBOLS);
            layer_off(_NUMPAD);
            layer_off(_UTIL);
            layer_on(_PLOVER);
            if (!eeconfig_is_enabled()) {
                eeconfig_init();
            }
            keymap_config.raw = eeconfig_read_keymap();
            keymap_config.nkro = 1;
            eeconfig_update_keymap(keymap_config.raw);
          }
          return false;
          break;
        case EXT_PLV:
          if (record->event.pressed) {
            #ifdef AUDIO_ENABLE
              PLAY_SONG(plover_gb_song);
            #endif
            layer_off(_PLOVER);
          }
          return false;
          break;
      }
  }
  return true;
}

bool     muse_mode      = false;
uint8_t  last_muse_note = 0;
uint16_t muse_counter   = 0;
uint8_t  muse_offset    = 70;
uint16_t muse_tempo     = 50;

bool dip_switch_update_user(uint8_t index, bool active) {
    switch (index) {
        case 0: {
#ifdef AUDIO_ENABLE
            static bool play_sound = false;
#endif
            if (active) {
#ifdef AUDIO_ENABLE
                if (play_sound) { PLAY_SONG(plover_song); }
#endif
                layer_on(_UTIL);
            } else {
#ifdef AUDIO_ENABLE
                if (play_sound) { PLAY_SONG(plover_gb_song); }
#endif
                layer_off(_UTIL);
            }
#ifdef AUDIO_ENABLE
            play_sound = true;
#endif
            break;
        }
        case 1:
            if (active) {
                muse_mode = true;
            } else {
                muse_mode = false;
            }
    }
    return true;
}

void matrix_scan_user(void) {
#ifdef AUDIO_ENABLE
    if (muse_mode) {
        if (muse_counter == 0) {
            uint8_t muse_note = muse_offset + SCALE[muse_clock_pulse()];
            if (muse_note != last_muse_note) {
                stop_note(compute_freq_for_midi_note(last_muse_note));
                play_note(compute_freq_for_midi_note(muse_note), 0xF);
                last_muse_note = muse_note;
            }
        }
        muse_counter = (muse_counter + 1) % muse_tempo;
    } else {
        if (muse_counter) {
            stop_all_notes();
            muse_counter = 0;
        }
    }
#endif
}

bool music_mask_user(uint16_t keycode) {
  switch (keycode) {
    default:
      return true;
  }
}

layer_state_t layer_state_set_user(layer_state_t state) {
    layer_set_rgb(state);
    return state;
}
